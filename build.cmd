@echo off

rem ####################################################################################################################
rem Config parameters below determine which versions of dependencies to use. Modify with caution!
rem ####################################################################################################################

set PHP_DESKTOP_URL=https://github.com/cztomczak/phpdesktop/releases/download/chrome-v57.0-rc/phpdesktop-chrome-57.0-rc-php-7.1.3.zip
set PHP_URL=https://windows.php.net/downloads/releases/php-7.3.27-Win32-VC15-x64.zip
set LIBCOAP_URL=https://github.com/obgm/libcoap/archive/develop.zip
set PHP_APPLICATION_URL=https://bitbucket.org/garrcomm/tradfri-web-application/get/master.zip
set COMPOSER_URL=https://getcomposer.org/download/2.0.12/composer.phar
set OPENSSL_INSTALL_PATH=%ProgramFiles%\OpenSSL-Win64
set INNO_SETUP_PATH=%ProgramFiles(x86)%\Inno Setup 6

rem ####################################################################################################################
rem Script starts here, there should be no need to modify this.
rem ####################################################################################################################

if not exist "%INNO_SETUP_PATH%\iscc.exe" (
    echo Inno Setup not found. Please install this first.
    pause
    goto :eof
)
if not exist "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\Common7\IDE\devenv.exe" (
    echo Correct Visual Studio development environment not found.
    echo Install Visual Studio 2019 CE with workload "Desktop development with C++" first.
    pause
    goto :eof
)
if not exist "%OPENSSL_INSTALL_PATH%\include\openssl\ssl.h" (
    echo OpenSSL not found. Please install Win64 OpenSSL v1.1.1k including development files.
    pause
    goto :eof
)
curl --version 1>nul 2>&1
if errorlevel 1 (
    echo cURL not found. Please update to Windows 10 build 17063 or newer.
    pause
    goto :eof
)
tar --help 1>nul 2>&1
if errorlevel 1 (
    echo tar not found. Please update to Windows 10 build 17063 or newer.
    pause
    goto :eof
)
xcopy /? 1>nul 2>&1
if errorlevel 1 (
    echo xcopy not found. Please update to Windows 10 build 17063 or newer.
    pause
    goto :eof
)

echo Creating directory structure required to build
mkdir downloads
mkdir downloads\php
mkdir build
mkdir build\locales

echo Downloading and extracting lib-coap
if not exist downloads\libcoap.zip curl -L %LIBCOAP_URL% --output downloads\libcoap.zip
tar -xf downloads\libcoap.zip -C downloads
move downloads\libcoap-* downloads\libcoap
mkdir downloads\libcoap\win32\lib

echo Building lib-coap for Windows
xcopy "%OPENSSL_INSTALL_PATH%\include" downloads\libcoap\include /s /y
xcopy "%OPENSSL_INSTALL_PATH%\lib" downloads\libcoap\win32\lib /s /y
"%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\Common7\IDE\devenv.exe" "downloads\libcoap\win32\libcoap.sln" /Build "Release DLL" /Project "libcoap" /Project "coap-client"
set LIBCOAP_BIN_PATH=%CD%\downloads\libcoap\win32\x64\Release DLL
if not exist "%LIBCOAP_BIN_PATH%\coap-client.exe" (
    echo Could not compile lib-coap.
    type "downloads\libcoap\win32\x64\Release DLL\libcoap.log"
    pause
    goto :eof
)

echo Downloading and extracting PHP Desktop to tmp folder
if not exist downloads\phpdesktop.zip curl -L %PHP_DESKTOP_URL% --output downloads\phpdesktop.zip
tar -xf downloads\phpdesktop.zip -C downloads
move downloads\phpdesktop-chrome-* downloads\phpdesktop

echo Downloading and extracting PHP to tmp folder
if not exist downloads\php.zip curl -L %PHP_URL% --output downloads\php.zip
tar -xf downloads\php.zip -C downloads\php

echo Downloading and extracting application to tmp folder
if not exist downloads\app.zip curl -L %PHP_APPLICATION_URL% --output downloads\app.zip
tar -xf downloads\app.zip -C downloads
move downloads\garrcomm-tradfri-* downloads\app

echo Downloading composer to tmp folder
if not exist downloads\composer.phar curl -L %COMPOSER_URL% --output downloads\composer.phar

echo Cherrypicking required files to build folder
copy downloads\phpdesktop\*.* build
copy downloads\phpdesktop\locales\en-*.pak build\locales
xcopy downloads\php\*.* build\win32 /s /y /i
copy downloads\composer.phar build\composer.phar
xcopy downloads\app\*.* build /s /y /i
copy "%LIBCOAP_BIN_PATH%\coap-client.exe" build\win32\coap-client.exe
copy "%LIBCOAP_BIN_PATH%\libcoap-2-openssl.dll" build\win32\libcoap-2-openssl.dll

echo Running composer in build folder
cd build
win32\php.exe composer.phar install --no-dev --prefer-dist --optimize-autoloader
cd ..

echo Placing artifacts
copy artifacts\settings.json build\settings.json
copy artifacts\security.ini build\config\security.ini

echo Cleaning up downloaded dirs and files
del /f /q /s downloads\phpdesktop\*.*
rmdir /q /s downloads\phpdesktop
del /f /q /s downloads\php\*.*
rmdir /q /s downloads\php
del /f /q /s downloads\app\*.*
rmdir /q /s downloads\app
del /f /q /s downloads\libcoap\*.*
rmdir /q /s downloads\libcoap

rem goto :eof

echo Building setup
"%INNO_SETUP_PATH%\iscc.exe" /O"%CD%" mysetup.iss

echo Cleaning up build dir
del /f /q /s build
rmdir /q /s build

pause

rem @TODO Upload binary to Bitbucket automatically with https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/downloads#post

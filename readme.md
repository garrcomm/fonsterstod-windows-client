# FÖNSTERSTÖD

I'm a big fan of the IKEA Home Smart system (TRÅDFRI).
But one thing was really missing, a Windows application to drive the system.
For that reason I started working on a Windows app, called FÖNSTERSTÖD (Swedish for Window Support according to translation software)

This repository contains the setup builder.  
**If you need the installer itself, just visit [fonsterstod.app](https://fonsterstod.app/) and follow directions there!**

## Purpose of this repository

This repository is mainly for my own use to build the Windows app and installer.
But since the app is open source, I also wanted to keep this builder open source.
Keep in mind though that this repository has no support whatsoever.

## Build the installer

To build the installer, a few dependencies need to be installed first;

* [Inno Setup 6.1.2](https://jrsoftware.org/isdl.php)  
  Inno Setup actually builds the installer based on the `mysetup.iss` script.
* [Visual Studio 2019 Community Edition](https://visualstudio.microsoft.com/downloads/) with the workload "*Desktop development with C++*".  
  We need Visual Studio to compile lib-coap for Windows.
* [Win64 OpenSSL v1.1.1k](https://slproweb.com/products/Win32OpenSSL.html) (*not the Light version! We need the development files as well*)  
  When we want to compile lib-coap, we need to include OpenSSL so the CoAPS protocol can be supported.
  
After that, it's a matter of running `build.cmd` to build the installer.
